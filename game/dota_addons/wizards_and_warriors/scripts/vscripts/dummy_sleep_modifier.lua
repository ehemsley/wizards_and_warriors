dummy_sleep_modifier = class({})

function dummy_sleep_modifier:IsDebuff()
	return true
end

function dummy_sleep_modifier:IsStunDebuff()
	return true
end

function dummy_sleep_modifier:GetEffectName()
	return "particles/generic_gameplay/generic_sleep.vpcf"
end

function dummy_sleep_modifier:GetEffectAttachType()
	return PATTACH_OVERHEAD_FOLLOW
end

function dummy_sleep_modifier:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}

	return funcs
end

function dummy_sleep_modifier:GetOverrideAnimation( params )
	return ACT_DOTA_DISABLED
end

function dummy_sleep_modifier:CheckState()
	local state = {
    [MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_STUNNED] = true,
		[MODIFIER_STATE_INVULNERABLE] = true,
	}

	return state
end
