"DOTAAbilities"
{
  // Explosive Tome
  "item_explosive_tome"
  {
    "ID"        "3001"
    "BaseClass"     "item_datadriven"
    "AbilityTextureName"    "item_necronomicon"
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_CHANNELED"
    "AbilityCastAnimation"    "ACT_DOTA_TELEPORT"

    "ItemDroppable"       "1"
    "ItemSellable"        "0"
    "ItemKillable"        "0"
    "ItemShareability"    "ITEM_FULLY_SHAREABLE"

    "AbilityCastRange"    "200"
    "AbilityCastPoint"    "0"
    "AbilityManaCost"     "0"

    "AbilityChannelTime"    "5"

    "OnAbilityPhaseStart"
    {
      "RunScript"
      {
        "ScriptFile"   "scripts/vscripts/items/explosive_tome.lua"
        "Function"     "check_location"
        "Target"       "CASTER"
      }
    }

    "OnChannelSucceeded"
    {
      "RunScript"
      {
        "ScriptFile"    "scripts/vscripts/items/explosive_tome.lua"
        "Function"      "spawn"
        "Target"        "CASTER"
      }
    }

    "OnOwnerDied"
    {
      "RunScript"
      {
        "ScriptFile"    "scripts/vscripts/items/item_functions.lua"
        "Function"      "DropItemOnDeath"
      }
    }
  }

  "item_defusal_wand"
  {
    "ID"    "3002"
    "BaseClass"     "item_datadriven"
    "AbilityTextureName"    "item_magic_wand"
    "AbilityBehavior"     "DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_CHANNELED"
    "AbilityUnitTargetType" "DOTA_UNIT_TARGET_BASIC"
    "AbilityUnitTargetTeam" "DOTA_UNIT_TARGET_TEAM_ENEMY"
    "AbilityUnitTargetFlags" "DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES | DOTA_UNIT_TARGET_FLAG_INVULNERABLE"

    "ItemDroppable"       "1"
    "ItemSellable"        "0"
    "ItemKillable"        "0"
    "ItemShareability"    "ITEM_FULLY_SHAREABLE"

    "AbilityCastRange"    "150"
    "AbilityCastPoint"    "0"
    "AbilityManaCost"     "0"

    "AbilityChannelTime"    "10"

    "OnChannelSucceeded"
    {
      "RunScript"
      {
        "ScriptFile"  "scripts/vscripts/items/defusal_wand.lua"
        "Function"    "destroy_tome"
        "Target"      "TARGET"
      }
    }

    "OnOwnerDied"
    {
      "RunScript"
      {
        "ScriptFile"    "scripts/vscripts/items/item_functions.lua"
        "Function"      "DropItemOnDeath"
      }
    }
  }

  //Shield
  "item_example_item"
  {
    "ID"              "1836"
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_DONT_RESUME_ATTACK"
    "AbilityUnitTargetTeam"     "DOTA_UNIT_TARGET_TEAM_ENEMY"
    "AbilityUnitTargetType"     "DOTA_UNIT_TARGET_HERO"
    "BaseClass"           "item_datadriven"
    "AbilityCastAnimation"      "ACT_DOTA_DISABLED"
    "AbilityTextureName"      "item_example_item"

    // Stats
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCastRange"        "900"
    "AbilityCastPoint"        "0.2"
    "AbilityCooldown"       "13.0"

    // Item Info
    //-------------------------------------------------------------------------------------------------------------
    "AbilityManaCost"       "100"
    "ItemCost"            "750"
    "ItemInitialCharges"      "0"
    "ItemDroppable"         "1"
    "ItemSellable"          "1"
    "ItemRequiresCharges"     "0"
    "ItemShareability"        "ITEM_NOT_SHAREABLE"
    "ItemDeclarations"        "DECLARE_PURCHASES_TO_TEAMMATES | DECLARE_PURCHASES_TO_SPECTATORS"

    "MaxUpgradeLevel"       "1"
    "ItemBaseLevel"         "1"

    "precache"
    {
      "particle"              "particles/frostivus_herofx/queen_shadow_strike_linear_parent.vpcf"
      "particle_folder"       "particles/test_particle"
      "soundfile"             "soundevents/game_sounds_heroes/game_sounds_abaddon.vsndevts"
    }
    "OnSpellStart"
    {
      "LinearProjectile"
      {
        "EffectName"      "particles/frostivus_herofx/queen_shadow_strike_linear_parent.vpcf"
        "MoveSpeed"       "%speed"
        //"StartPosition"   "attach_attack1"
        "FixedDistance"   "%distance"
        "StartRadius"     "%radius"
        "EndRadius"       "%radius"
        "TargetTeams"     "DOTA_UNIT_TARGET_TEAM_ENEMY"
        "TargetTypes"     "DOTA_UNIT_TARGET_HERO"
        "TargetFlags"     "DOTA_UNIT_TARGET_FLAG_NONE"
        "HasFrontalCone"    "0"
        "ProvidesVision"    "0"
        "VisionRadius"      "0"
      }
      "FireSound"
      {
        "EffectName"    "Hero_Abaddon.AphoticShield.Cast"
        "Target"        "CASTER"
      }
      "ApplyModifier"
      {
        "Target"      "CASTER"
        "ModifierName"  "modifier_item_shield"
      }
    }

    "OnProjectileHitUnit"
    {
      "DeleteOnHit" "0"
      "Damage"
      {
        "Target"      "TARGET"
        "Type"    "DAMAGE_TYPE_PURE"
        "Damage"  "%damage"
      }
    }

    "Modifiers"
    {
      "modifier_item_shield"
      {
        "EffectName"    "particles/test_particle/damage_immunity.vpcf"
        "EffectAttachType"  "follow_origin"
        "Target"      "CASTER"

        "Duration" "%duration"
        "TextureName" "abaddon_aphotic_shield"
        "Properties"
        {
          "MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE"    "%damage_reduction"
        }
      }
    }

    // Special
    //-------------------------------------------------------------------------------------------------------------
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"        "FIELD_FLOAT"
        "duration"        "4.0"
      }

      "02"
      {
        "var_type"        "FIELD_INTEGER"
        "damage_reduction"    "-50"
      }
      "03"
      {
        "var_type"        "FIELD_INTEGER"
        "radius"          "150"
      }
      "04"
      {
        "var_type"        "FIELD_INTEGER"
        "speed"           "1800"
      }
      "05"
      {
        "var_type"        "FIELD_FLOAT"
        "distance"        "900"
      }
      "06"
      {
        "var_type"        "FIELD_INTEGER"
        "damage"          "125"
      }
    }
  }
}
